package com.federico.teotini;

public class Main {

    private static int number = 1;

    public static void main(String[] args) {

//      Base Price: 30
//      Child Price: -1.5
//      Army Price: -3
//      Elderly Price: -3
//      Student Price: -4.5


        System.out.println(">>>>Working example");
        Ticket ticket = new Ticket("Federico", 25, "student");
        ticket.print();

        System.out.println("\n\n>>>>Begin tests");
        testTicket("Base Ticket, the price should be 30", 25, null, 30);
        testTicket("Child Ticket, the price should be 28.5", 10, null, 28.5);
        testTicket("Elderly Ticket, the price should be 27", 70, null, 27);
        testTicket("Elderly Ticket, was in the army, the price should be 24", 70, "army", 24);
        testTicket("Student Ticket, the price should be 25.5", 25, "student", 25.5);
        testTicket("Student Child Ticket, the price should be 24", 10, "student", 24);
    }

    private static void testTicket(String title, int age, String discountClass, double expectedPrice) {
        System.out.println("\nTest " + number + ": " + title);
        Ticket ticket = new Ticket("Federico", age, discountClass);
        System.out.println("Price is "+ ticket.getPrice());
        if (ticket.getPrice() == expectedPrice) {
            System.out.println("Ok");
        } else {
            System.out.println("Error");
        }

        number++;
    }
}
