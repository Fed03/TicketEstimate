package com.federico.teotini.ticketPrice.builder;

import com.federico.teotini.ticketPrice.*;

public final class PriceBuilder {
    private TicketPrice result;
    private final int age;
    private final String discountClass;

    public PriceBuilder(int age, String discountClass) {
        this.age = age;
        this.discountClass = discountClass;
    }

    public PriceBuilder(int age) {
        this.age = age;
        discountClass = null;
    }

    public void buildTicketPrice() {
        result = new BasePrice();
        applyAgeDiscount();
        applyClassDiscount();
    }

    private void applyClassDiscount() {
        if (discountClass == "student") {
            result = new StudentPrice(result);
        } else if (discountClass == "army") {
            result = new ArmyPrice(result);
        }
    }

    private void applyAgeDiscount() {
        if (age <= 12) {
            result = new ChildPrice(result);
        } else if (age >= 65) {
            result = new ElderlyPrice(result);
        }
    }

    public TicketPrice getResult() {
        return result;
    }
}
