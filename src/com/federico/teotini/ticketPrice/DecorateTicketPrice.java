package com.federico.teotini.ticketPrice;

public abstract class DecorateTicketPrice implements TicketPrice {
    TicketPrice price;

    public DecorateTicketPrice(TicketPrice price) {
        this.price = price;
    }

    public double getPrice() {

        return price.getPrice() - discountAmount();
    }

    abstract double discountAmount();
}
