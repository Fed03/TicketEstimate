package com.federico.teotini.ticketPrice;

public final class ElderlyPrice extends DecorateTicketPrice {
    public ElderlyPrice(TicketPrice price) {
        super(price);
    }

    @Override
    double discountAmount() {
        return 3;
    }

}
