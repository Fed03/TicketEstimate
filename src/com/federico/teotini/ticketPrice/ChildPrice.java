package com.federico.teotini.ticketPrice;

public final class ChildPrice extends DecorateTicketPrice {
    public ChildPrice(TicketPrice price) {
        super(price);
    }

    @Override
    double discountAmount() {
        return 1.5;
    }
}
