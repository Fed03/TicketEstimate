package com.federico.teotini.ticketPrice;

public final class StudentPrice extends DecorateTicketPrice {
    public StudentPrice(TicketPrice price) {
        super(price);
    }

    @Override
    double discountAmount() {
        return 4.5;
    }

}
