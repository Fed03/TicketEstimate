package com.federico.teotini.ticketPrice;

public interface TicketPrice {
    public double getPrice();
}
