package com.federico.teotini.ticketPrice;

public final class ArmyPrice extends DecorateTicketPrice {
    public ArmyPrice(TicketPrice price) {
        super(price);
    }

    @Override
    double discountAmount() {
        return 3;
    }


}
