package com.federico.teotini;

import com.federico.teotini.ticketPrice.TicketPrice;
import com.federico.teotini.ticketPrice.builder.PriceBuilder;

public class Ticket {
    private TicketPrice price;
    private final String fullname;
    private final int age;
    private final String discountClass;

    public Ticket(String name, int age, String discountClass) {
        fullname = name;
        this.age = age;
        this.discountClass = discountClass;

        PriceBuilder priceBuilder = new PriceBuilder(this.age, this.discountClass);
        priceBuilder.buildTicketPrice();
        price = priceBuilder.getResult();
    }

    public Ticket(String name, int age) {
        fullname = name;
        this.age = age;
        discountClass = null;

        PriceBuilder priceBuilder = new PriceBuilder(this.age);
        priceBuilder.buildTicketPrice();
        price = priceBuilder.getResult();
    }

    public double getPrice() {
        return this.price.getPrice();
    }

    public void print() {
        System.out.println("--- Ticket ---");
        System.out.println(this.fullname);
        System.out.println("Age: " + this.age);
        if (discountClass != null && !discountClass.isEmpty()) {
            System.out.println("Discount applied: " + discountClass);
        }
        System.out.println("Price: " + price.getPrice());
    }
}
